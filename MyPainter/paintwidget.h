#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QVector>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>


class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	void DDA(double polomer, int pocet, int red, int green, int blue);
	void Raster(int x1, int y1, int x2, int y2);
	void Raster_steep(int x1, int y1, int x2, int y2);
	void Bresenhamov(double polomer, int pocet, int red, int green, int blue);
	void Raster_b(int x1, int y1, int x2, int y2);
	void Raster_b_steep(int x1, int y1, int x2, int y2);
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void Kresli_obrys(int red, int green, int blue);
	void Vyplnaj(int red, int green, int blue);
	int Nacitaj_hrany();
	void Posun(int red, int green, int blue, int x_posun, int y_posun);
	void Rotate(int red, int green, int blue, double uhol);
	void Skaluj(int red, int green, int blue, double param);
	void Skos(int red, int green, int blue, int index, double koef);
	void Prevrat(int red, int green, int blue, int index);

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QRgb value;
	QList<QPoint> vrcholy;
	QList<QVector<float>> hrany;
	QList<QVector<float>> aktivne_hrany;
};

#endif // PAINTWIDGET_H
