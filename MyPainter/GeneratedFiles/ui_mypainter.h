/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QGridLayout *gridLayout;
    QPushButton *kresli_button;
    QPushButton *Clear_button;
    QGroupBox *groupBox_6;
    QFormLayout *formLayout_7;
    QLabel *label_9;
    QLabel *label_10;
    QPushButton *Skos_button;
    QComboBox *Smer_skosenia;
    QDoubleSpinBox *Koeficient_skosenia;
    QGroupBox *groupBox_3;
    QFormLayout *formLayout_4;
    QLabel *label_7;
    QLabel *label_8;
    QSpinBox *Posunutie_x;
    QSpinBox *Posunutie_y;
    QPushButton *Posun_button;
    QGroupBox *groupBox_4;
    QFormLayout *formLayout_5;
    QLabel *label_11;
    QDoubleSpinBox *Uhol_otocenia;
    QPushButton *Rotuj_button;
    QGroupBox *groupBox;
    QFormLayout *formLayout_2;
    QLabel *label_4;
    QSpinBox *red_v;
    QLabel *label_5;
    QSpinBox *green_v;
    QLabel *label_6;
    QSpinBox *blue_v;
    QGroupBox *groupBox_2;
    QFormLayout *formLayout_3;
    QLabel *label;
    QSpinBox *red_h;
    QLabel *label2;
    QSpinBox *green_h;
    QLabel *label3;
    QSpinBox *blue_h;
    QGroupBox *groupBox_5;
    QFormLayout *formLayout_6;
    QLabel *label_12;
    QDoubleSpinBox *Parameter_skalovania;
    QPushButton *Skaluj_button;
    QGroupBox *groupBox_7;
    QFormLayout *formLayout_8;
    QLabel *label_13;
    QComboBox *Os_preklopenia;
    QPushButton *Preklop_button;
    QGridLayout *gridLayout_2;
    QComboBox *algoritmus;
    QPushButton *kresli__kruh_button;
    QLabel *label_2;
    QLabel *label_3;
    QSpinBox *pocet;
    QDoubleSpinBox *polomer;
    QLabel *label_17;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(671, 644);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 318, 581));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        kresli_button = new QPushButton(centralWidget);
        kresli_button->setObjectName(QStringLiteral("kresli_button"));

        gridLayout->addWidget(kresli_button, 7, 0, 1, 1);

        Clear_button = new QPushButton(centralWidget);
        Clear_button->setObjectName(QStringLiteral("Clear_button"));

        gridLayout->addWidget(Clear_button, 7, 1, 1, 1);

        groupBox_6 = new QGroupBox(centralWidget);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        formLayout_7 = new QFormLayout(groupBox_6);
        formLayout_7->setSpacing(6);
        formLayout_7->setContentsMargins(11, 11, 11, 11);
        formLayout_7->setObjectName(QStringLiteral("formLayout_7"));
        label_9 = new QLabel(groupBox_6);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout_7->setWidget(0, QFormLayout::LabelRole, label_9);

        label_10 = new QLabel(groupBox_6);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout_7->setWidget(1, QFormLayout::LabelRole, label_10);

        Skos_button = new QPushButton(groupBox_6);
        Skos_button->setObjectName(QStringLiteral("Skos_button"));

        formLayout_7->setWidget(2, QFormLayout::SpanningRole, Skos_button);

        Smer_skosenia = new QComboBox(groupBox_6);
        Smer_skosenia->setObjectName(QStringLiteral("Smer_skosenia"));

        formLayout_7->setWidget(0, QFormLayout::FieldRole, Smer_skosenia);

        Koeficient_skosenia = new QDoubleSpinBox(groupBox_6);
        Koeficient_skosenia->setObjectName(QStringLiteral("Koeficient_skosenia"));
        Koeficient_skosenia->setMinimum(-1);
        Koeficient_skosenia->setMaximum(1);
        Koeficient_skosenia->setSingleStep(0.01);
        Koeficient_skosenia->setValue(0.4);

        formLayout_7->setWidget(1, QFormLayout::FieldRole, Koeficient_skosenia);


        gridLayout->addWidget(groupBox_6, 3, 1, 1, 1);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        formLayout_4 = new QFormLayout(groupBox_3);
        formLayout_4->setSpacing(6);
        formLayout_4->setContentsMargins(11, 11, 11, 11);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_7);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_8);

        Posunutie_x = new QSpinBox(groupBox_3);
        Posunutie_x->setObjectName(QStringLiteral("Posunutie_x"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, Posunutie_x);

        Posunutie_y = new QSpinBox(groupBox_3);
        Posunutie_y->setObjectName(QStringLiteral("Posunutie_y"));
        Posunutie_y->setSingleStep(1);

        formLayout_4->setWidget(1, QFormLayout::FieldRole, Posunutie_y);

        Posun_button = new QPushButton(groupBox_3);
        Posun_button->setObjectName(QStringLiteral("Posun_button"));

        formLayout_4->setWidget(2, QFormLayout::SpanningRole, Posun_button);


        gridLayout->addWidget(groupBox_3, 3, 0, 1, 1);

        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        formLayout_5 = new QFormLayout(groupBox_4);
        formLayout_5->setSpacing(6);
        formLayout_5->setContentsMargins(11, 11, 11, 11);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_11);

        Uhol_otocenia = new QDoubleSpinBox(groupBox_4);
        Uhol_otocenia->setObjectName(QStringLiteral("Uhol_otocenia"));
        Uhol_otocenia->setMinimum(-360);
        Uhol_otocenia->setMaximum(360);

        formLayout_5->setWidget(0, QFormLayout::FieldRole, Uhol_otocenia);

        Rotuj_button = new QPushButton(groupBox_4);
        Rotuj_button->setObjectName(QStringLiteral("Rotuj_button"));

        formLayout_5->setWidget(1, QFormLayout::SpanningRole, Rotuj_button);


        gridLayout->addWidget(groupBox_4, 4, 0, 1, 1);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout_2 = new QFormLayout(groupBox);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_4);

        red_v = new QSpinBox(groupBox);
        red_v->setObjectName(QStringLiteral("red_v"));
        red_v->setMaximum(255);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, red_v);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_5);

        green_v = new QSpinBox(groupBox);
        green_v->setObjectName(QStringLiteral("green_v"));
        green_v->setMaximum(255);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, green_v);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_6);

        blue_v = new QSpinBox(groupBox);
        blue_v->setObjectName(QStringLiteral("blue_v"));
        blue_v->setMaximum(255);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, blue_v);


        gridLayout->addWidget(groupBox, 1, 1, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        formLayout_3 = new QFormLayout(groupBox_2);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label);

        red_h = new QSpinBox(groupBox_2);
        red_h->setObjectName(QStringLiteral("red_h"));
        red_h->setMaximum(255);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, red_h);

        label2 = new QLabel(groupBox_2);
        label2->setObjectName(QStringLiteral("label2"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label2);

        green_h = new QSpinBox(groupBox_2);
        green_h->setObjectName(QStringLiteral("green_h"));
        green_h->setMaximum(255);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, green_h);

        label3 = new QLabel(groupBox_2);
        label3->setObjectName(QStringLiteral("label3"));

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label3);

        blue_h = new QSpinBox(groupBox_2);
        blue_h->setObjectName(QStringLiteral("blue_h"));
        blue_h->setMaximum(255);

        formLayout_3->setWidget(3, QFormLayout::FieldRole, blue_h);


        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        formLayout_6 = new QFormLayout(groupBox_5);
        formLayout_6->setSpacing(6);
        formLayout_6->setContentsMargins(11, 11, 11, 11);
        formLayout_6->setObjectName(QStringLiteral("formLayout_6"));
        label_12 = new QLabel(groupBox_5);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_12);

        Parameter_skalovania = new QDoubleSpinBox(groupBox_5);
        Parameter_skalovania->setObjectName(QStringLiteral("Parameter_skalovania"));
        Parameter_skalovania->setMinimum(-3);
        Parameter_skalovania->setMaximum(3);
        Parameter_skalovania->setSingleStep(0.1);

        formLayout_6->setWidget(0, QFormLayout::FieldRole, Parameter_skalovania);

        Skaluj_button = new QPushButton(groupBox_5);
        Skaluj_button->setObjectName(QStringLiteral("Skaluj_button"));

        formLayout_6->setWidget(1, QFormLayout::SpanningRole, Skaluj_button);


        gridLayout->addWidget(groupBox_5, 4, 1, 1, 1);

        groupBox_7 = new QGroupBox(centralWidget);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        formLayout_8 = new QFormLayout(groupBox_7);
        formLayout_8->setSpacing(6);
        formLayout_8->setContentsMargins(11, 11, 11, 11);
        formLayout_8->setObjectName(QStringLiteral("formLayout_8"));
        label_13 = new QLabel(groupBox_7);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout_8->setWidget(1, QFormLayout::LabelRole, label_13);

        Os_preklopenia = new QComboBox(groupBox_7);
        Os_preklopenia->setObjectName(QStringLiteral("Os_preklopenia"));

        formLayout_8->setWidget(1, QFormLayout::FieldRole, Os_preklopenia);

        Preklop_button = new QPushButton(groupBox_7);
        Preklop_button->setObjectName(QStringLiteral("Preklop_button"));

        formLayout_8->setWidget(2, QFormLayout::SpanningRole, Preklop_button);


        gridLayout->addWidget(groupBox_7, 5, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        algoritmus = new QComboBox(centralWidget);
        algoritmus->setObjectName(QStringLiteral("algoritmus"));

        gridLayout_2->addWidget(algoritmus, 4, 1, 1, 1);

        kresli__kruh_button = new QPushButton(centralWidget);
        kresli__kruh_button->setObjectName(QStringLiteral("kresli__kruh_button"));

        gridLayout_2->addWidget(kresli__kruh_button, 5, 0, 1, 2);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 4, 0, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 3, 0, 1, 1);

        pocet = new QSpinBox(centralWidget);
        pocet->setObjectName(QStringLiteral("pocet"));
        pocet->setMinimum(3);
        pocet->setMaximum(1000);

        gridLayout_2->addWidget(pocet, 3, 1, 1, 1);

        polomer = new QDoubleSpinBox(centralWidget);
        polomer->setObjectName(QStringLiteral("polomer"));

        gridLayout_2->addWidget(polomer, 2, 1, 1, 1);

        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_2->addWidget(label_17, 2, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 5, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout);


        verticalLayout->addLayout(horizontalLayout);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 671, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(Clear_button, SIGNAL(clicked()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(kresli_button, SIGNAL(clicked()), MyPainterClass, SLOT(Kresli_click()));
        QObject::connect(Posun_button, SIGNAL(clicked()), MyPainterClass, SLOT(Posun_click()));
        QObject::connect(Skaluj_button, SIGNAL(clicked()), MyPainterClass, SLOT(Skaluj_click()));
        QObject::connect(Skos_button, SIGNAL(clicked()), MyPainterClass, SLOT(Skos_click()));
        QObject::connect(Rotuj_button, SIGNAL(clicked()), MyPainterClass, SLOT(Rotuj_click()));
        QObject::connect(Preklop_button, SIGNAL(clicked()), MyPainterClass, SLOT(Preklop_click()));
        QObject::connect(kresli__kruh_button, SIGNAL(clicked()), MyPainterClass, SLOT(Kresli_kruh()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        kresli_button->setText(QApplication::translate("MyPainterClass", "Kresli", 0));
        Clear_button->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        groupBox_6->setTitle(QApplication::translate("MyPainterClass", "Skosenie", 0));
        label_9->setText(QApplication::translate("MyPainterClass", "Smer skosenia", 0));
        label_10->setText(QApplication::translate("MyPainterClass", "Koeficient", 0));
        Skos_button->setText(QApplication::translate("MyPainterClass", "Skos", 0));
        Smer_skosenia->clear();
        Smer_skosenia->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "x", 0)
         << QApplication::translate("MyPainterClass", "y", 0)
        );
        groupBox_3->setTitle(QApplication::translate("MyPainterClass", "Posunutie", 0));
        label_7->setText(QApplication::translate("MyPainterClass", " x ", 0));
        label_8->setText(QApplication::translate("MyPainterClass", " y ", 0));
        Posun_button->setText(QApplication::translate("MyPainterClass", "Posun", 0));
        groupBox_4->setTitle(QApplication::translate("MyPainterClass", "Rotacia", 0));
        label_11->setText(QApplication::translate("MyPainterClass", "Uhol", 0));
        Uhol_otocenia->setSuffix(QApplication::translate("MyPainterClass", " \302\260", 0));
        Rotuj_button->setText(QApplication::translate("MyPainterClass", "Rotuj", 0));
        groupBox->setTitle(QApplication::translate("MyPainterClass", "Vypln", 0));
        label_4->setText(QApplication::translate("MyPainterClass", "Red", 0));
        label_5->setText(QApplication::translate("MyPainterClass", "Green", 0));
        label_6->setText(QApplication::translate("MyPainterClass", "Blue", 0));
        groupBox_2->setTitle(QApplication::translate("MyPainterClass", "Hranica", 0));
        label->setText(QApplication::translate("MyPainterClass", "Red", 0));
        label2->setText(QApplication::translate("MyPainterClass", "Green", 0));
        label3->setText(QApplication::translate("MyPainterClass", "Blue", 0));
        groupBox_5->setTitle(QApplication::translate("MyPainterClass", "Skalovanie", 0));
        label_12->setText(QApplication::translate("MyPainterClass", "Parameter ", 0));
        Skaluj_button->setText(QApplication::translate("MyPainterClass", "Skaluj", 0));
        groupBox_7->setTitle(QApplication::translate("MyPainterClass", "Preklopenie", 0));
        label_13->setText(QApplication::translate("MyPainterClass", "Os ", 0));
        Os_preklopenia->clear();
        Os_preklopenia->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "Os x", 0)
         << QApplication::translate("MyPainterClass", "Os y", 0)
         << QApplication::translate("MyPainterClass", "Priamka y = x", 0)
         << QApplication::translate("MyPainterClass", "Priamka y = -x", 0)
        );
        Preklop_button->setText(QApplication::translate("MyPainterClass", "Preklop", 0));
        algoritmus->clear();
        algoritmus->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "DDA", 0)
         << QApplication::translate("MyPainterClass", "Bresenhamov", 0)
        );
        kresli__kruh_button->setText(QApplication::translate("MyPainterClass", "Kresli kruh", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Algoritmus", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "Pocet bodov", 0));
        label_17->setText(QApplication::translate("MyPainterClass", "Polomer kruznice", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
