#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);

	ui.Posunutie_x->setMinimum(-paintWidget.width());
	ui.Posunutie_x->setMaximum(paintWidget.width());
	ui.Posunutie_y->setMinimum(-paintWidget.height());
	ui.Posunutie_y->setMaximum(paintWidget.height());
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Kresli_click()
{
	paintWidget.Kresli_obrys(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value());
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Posun_click()
{
	paintWidget.Posun(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value(), ui.Posunutie_x->value(), ui.Posunutie_y->value());
	ui.Posunutie_x->setValue(0.0);
	ui.Posunutie_y->setValue(0.0);
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Rotuj_click()
{
	paintWidget.Rotate(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value(), ui.Uhol_otocenia->value());
	ui.Uhol_otocenia->setValue(5.0);
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Skaluj_click()
{
	paintWidget.Skaluj(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value(), ui.Parameter_skalovania->value());
	ui.Parameter_skalovania->setValue(0.0);
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Preklop_click()
{
	paintWidget.Prevrat(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value(), ui.Os_preklopenia->currentIndex());
	ui.Os_preklopenia->setCurrentIndex(0);
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Skos_click()
{
	paintWidget.Skos(ui.red_h->value(), ui.green_h->value(), ui.blue_h->value(), ui.Smer_skosenia->currentIndex(), ui.Koeficient_skosenia->value());
	ui.Smer_skosenia->setCurrentIndex(0);
	ui.Koeficient_skosenia->setValue(0.4);
	paintWidget.Vyplnaj(ui.red_v->value(), ui.green_v->value(), ui.blue_v->value());
}

void MyPainter::Kresli_kruh()
{
	if (ui.algoritmus->currentIndex() == 0)
		paintWidget.DDA(ui.polomer->value(), ui.pocet->value(), ui.red_h->value(), ui.green_h->value(), ui.blue_h->value());
	else
		paintWidget.Bresenhamov(ui.polomer->value(), ui.pocet->value(), ui.red_h->value(), ui.green_h->value(), ui.blue_h->value());
}
