#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
	newImage(400, 400);
}

void PaintWidget::DDA(double polomer, int pocet, int red, int green, int blue)
{
	value = qRgb(red,green,blue);
	polomer = 5 * polomer;
	newImage(50 + 2 * polomer, 50 + 2 * polomer);
	double dielik = 2*M_PI / pocet;
	double stred = (50 + 2 * polomer) / 2;
	int x1 = stred + polomer, y1 = stred, x2, y2;
	
	for (double i = 0 ; i <= 2* M_PI ; i += dielik)
	{
		x2 =(int)round (stred + polomer * cos(i));
		y2 = (int)round (stred + polomer * sin(i));

		if (x1 > x2)
			Raster(x2, y2, x1, y1);
		else
			Raster(x1, y1, x2, y2);	
		x1 = x2;
		y1 = y2;
	}	
	update();
}

void PaintWidget::Raster(int x1, int y1, int x2, int y2)
{
	double y = y1;
	double m = (double)(y2 - y1) / (x2 - x1);

	if (abs(m) <= 1)
	{
		for (int i = x1; i <= x2; i++)
		{
			image.setPixel( i, (int)round(y),value);
			y += m;
		}
	}
	else
		Raster_steep(y1, x1, y2, x2);
}
void PaintWidget::Raster_steep(int x1, int y1, int x2, int y2)
{
	if (x1 > x2) 
	{ 
		Raster_steep(x2, y2, x1, y1); 
		return;
	}

	double y = y1;         
	double m = (double)(y2 - y1) / (x2 - x1);

	for (int i = x1; i <= x2; i++)
	{
		image.setPixel( (int)round(y), i, value);
		y += m;
	}
}

void PaintWidget::Bresenhamov(double polomer, int pocet, int red, int green, int blue)
{
	value = qRgb(red, green, blue);
	polomer = 5 * polomer;
	newImage(50 + 2 * polomer, 50 + 2 * polomer);
	double dielik = 2 * M_PI / (double)pocet;
	double stred = (50 + 2 * polomer) / 2.0;
	int x1 = stred + polomer, y1 = stred, x2, y2;

	for (double i = 0; i <= 2 * M_PI; i += dielik)
	{
		x2 = (int)round(stred + polomer * cos(i));
		y2 = (int)round(stred + polomer * sin(i));

		if (x1 > x2)
			Raster_b(x2, y2, x1, y1);
		else
			Raster_b(x1, y1, x2, y2);

		x1 = x2;
		y1 = y2;
	}
	update();
}

void PaintWidget::Raster_b(int x1, int y1, int x2, int y2)
{
	if (abs(y2 - y1) > abs(x2 - x1))
	{
		Raster_b_steep(y1, x1, y2, x2);
		return;
	}
	int dy = y2 - y1;
	int dx = x2 - x1;
	float m = dy / (float)dx;
	int k1 = 2 * dy;
	int k2 = m>0 ? (2 * dy - 2 * dx) : (2 * dy + 2 * dx);
	int	p = m>0 ? (2 * dy - dx) : (2 * dy + dx);
	int x = x1, y = y1;

	image.setPixel(x, y, value);
	while (x <= x2)
	{
		x++;
		if (p > 0)
		{
			y += m>0 ? 1 : 0;
			p += m>0 ? k2 : k1;
		}
		else
		{
			y -= m > 0 ? 0 : 1;
			p += m > 0 ? k1 : k2;
		}
		image.setPixel(x, y, value);
	}
}

void PaintWidget::Raster_b_steep(int x1, int y1, int x2, int y2)
{
	if ((x1 - x2) > 0)
	{
		Raster_b_steep(x2, y2, x1, y1);
		return;
	}
	int dy = y2 - y1;
	int dx = x2 - x1;
	float m = dy / (float)dx;
	int k1 = 2 * dy;
	int k2 = m>0 ? (2 * dy - 2 * dx) : (2 * dy + 2 * dx);
	int	p = m>0 ? (2 * dy - dx) : (2 * dy + dx);
	int x = x1, y = y1;

	image.setPixel(y, x, value);
	while (x <= x2)
	{
		x++;
		if (p > 0)
		{
			y += m>0 ? 1 : 0;
			p += m>0 ? k2 : k1;
		}
		else
		{
			y -= m > 0 ? 0 : 1;
			p += m > 0 ? k1 : k2;
		}
		image.setPixel(y, x, value);
	}
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::Kresli_obrys(int red, int green, int blue)
{
	value = qRgb(red, green, blue);
	int x1, x2, y1, y2;
	vrcholy << vrcholy[0];
	for (int i = 0; i < vrcholy.size()-1; i++)
	{
		x1 = vrcholy[i].x();
		y1 = vrcholy[i].y();
		x2 = vrcholy[i + 1].x();
		y2 = vrcholy[i + 1].y();
		if (x1 > x2)
			Raster(x2, y2, x1, y1);
		else
			Raster(x1, y1, x2, y2);
	}
	vrcholy.removeLast();
	update();
}

void PaintWidget::Vyplnaj(int red, int green, int blue)
{
	QVector<float> vektor;
	QVector<int> priesecniky;
	int xa,xb;
	value = qRgb(red, green, blue);
	
	int y_max = Nacitaj_hrany();

	for (int ya = hrany[0][1]; ya < y_max; ya++)
	{		
		for (int j = 0; j< hrany.size(); j++) {
			if (ya == hrany[j][1]) {
				aktivne_hrany << hrany[j];
			}

		}
		for (int j = 0; j < aktivne_hrany.size(); j ++) 
			priesecniky.push_back((int)round(aktivne_hrany[j][3] * ya - aktivne_hrany[j][3] * aktivne_hrany[j][1] + aktivne_hrany[j][0]));
	
		for (int i = 0; i < aktivne_hrany.size() - 1; i++) {	//triedenie TAH podla akt. x-ovych priesecnikov
			for (int j = 0; j < aktivne_hrany.size() - i - 1; j++) {
				if (priesecniky[j + 1] < priesecniky[j]) {
					vektor = aktivne_hrany[j + 1];
					aktivne_hrany[j + 1] = aktivne_hrany[j];
					aktivne_hrany[j] = vektor;
					int a = priesecniky[j + 1];
					priesecniky[j + 1] = priesecniky[j];
					priesecniky[j] = a;
				}
			}
		}
		for (int j = 0; j < aktivne_hrany.size()-1; j+=2) {
			xa = priesecniky[j];
			xb = priesecniky[j + 1];
			Raster(xa, ya, xb, ya);
			}
		for (int j=0; j < aktivne_hrany.size();j++) {
			if (aktivne_hrany[j][2] == ya) {
				aktivne_hrany.removeAt(j);
				j--;
			}
		}
		priesecniky.clear();
	}
	hrany.clear();
	aktivne_hrany.clear();
}

int PaintWidget::Nacitaj_hrany()
{
	int y_max = 0;
	QVector<float> vektor;
//vytvorenie TH	
	for (int i = 0; i < vrcholy.size(); i++)
	{
		if (vrcholy[i].y() > y_max)
			y_max = vrcholy[i].y();

		if (vrcholy[i].y() > vrcholy[i + 1].y()){
			vektor.push_back(vrcholy[i+1].x());
			vektor.push_back(vrcholy[i+1].y());
			vektor.push_back(vrcholy[i].y()-1);
			vektor.push_back((float)(vrcholy[i].x() - vrcholy[i+1].x()) / (vrcholy[i].y() - vrcholy[i+1].y()));
		}
		else {
			vektor.push_back(vrcholy[i].x());
			vektor.push_back(vrcholy[i].y());
			vektor.push_back(vrcholy[i + 1].y() - 1);
			vektor.push_back((float)(vrcholy[i + 1].x() - vrcholy[i].x()) / (vrcholy[i + 1].y() - vrcholy[i].y()));
		}
		hrany << vektor;
		vektor.clear();
	}
//triedenie TH
	for (int i = 0; i < hrany.size() - 1; i++) {
		for (int j = 0; j < hrany.size() - i - 1; j++) {
			if (hrany[j + 1][1] == hrany[j][1]) {
				if (hrany[j + 1][0] == hrany[j][0]) {
					if (hrany[j + 1][3] < hrany[j][3]) {
						vektor = hrany[j + 1];
						hrany[j + 1] = hrany[j];
						hrany[j] = vektor;
					}
				}
				else
					if (hrany[j + 1][0] < hrany[j][0]) {
						vektor = hrany[j + 1];
						hrany[j + 1] = hrany[j];
						hrany[j] = vektor;
					}
			}
			else
				if (hrany[j + 1][1] < hrany[j][1]) {
					vektor = hrany[j + 1];
					hrany[j + 1] = hrany[j];
					hrany[j] = vektor;
				}
		}
	}

	return y_max;
}

void PaintWidget::Posun(int red, int green, int blue, int x_posun, int y_posun)
{
	image.fill(qRgb(255, 255, 255));

	for (int i = 0; i < vrcholy.size(); i++)
		vrcholy[i] = QPoint(vrcholy[i].x() + x_posun, vrcholy[i].y() - y_posun);
	
	Kresli_obrys(red, green, blue);
}

void PaintWidget::Rotate(int red, int green, int blue, double uhol)
{
	int xx, yy;
	image.fill(qRgb(255, 255, 255));
	uhol = -(uhol * M_PI) / 180;
	for (int i = 1; i < vrcholy.size(); i++)
	{
		xx = (int)(vrcholy[i].x() - vrcholy[0].x()) * cos(uhol) - (vrcholy[i].y() - vrcholy[0].y()) * sin(uhol) + vrcholy[0].x();
		yy = (int)(vrcholy[i].x() - vrcholy[0].x())* sin(uhol) + (vrcholy[i].y() - vrcholy[0].y()) * cos(uhol) + vrcholy[0].y();
		vrcholy[i] = QPoint(xx, yy);
	}

	Kresli_obrys(red, green, blue);
}

void PaintWidget::Skaluj(int red, int green, int blue, double param)
{
	if (param == 0.0)
		return;
	QPoint bod, rel_bod;
	image.fill(qRgb(255, 255, 255));
	for (int i = 1; i < vrcholy.size(); i++)
	{
		bod = vrcholy[0];
		rel_bod = vrcholy[i] - bod;
		vrcholy[i] = vrcholy[0] + rel_bod * param;
	}

	Kresli_obrys(red, green, blue);
}

void PaintWidget::Skos(int red, int green, int blue, int index, double koef)
{
	if (koef == 0.0)
		return;

	int pov_vrchol_x = vrcholy[0].x(), pov_vrchol_y = vrcholy[0].y();
	image.fill(qRgb(255, 255, 255));

	if (index == 0)
		for (int i = 0; i < vrcholy.size(); i++)
			vrcholy[i] = QPoint(vrcholy[i].x() + koef * (image.height() - vrcholy[i].y()), vrcholy[i].y());
	else
		for (int i = 0; i < vrcholy.size(); i++)
			vrcholy[i] = QPoint(vrcholy[i].x(), vrcholy[i].y() + koef * (vrcholy[i].x() - image.width()));

	if (index == 0)
		Posun(red, green, blue, pov_vrchol_x - vrcholy[0].x(), 0);
	else
		Posun(red, green, blue, 0, vrcholy[0].y() - pov_vrchol_y);
}

void PaintWidget::Prevrat(int red, int green, int blue, int index)
{
	int a, b, c, xx, yy;
	image.fill(qRgb(255, 255, 255));
	value = qRgb(0, 0, 255);
	if (index == 0) {
		a = 0;
		b = 1;
		c = -image.height() / 2;
		Raster(0, -c, -c * 2 - 1, -c);
	}
	else if (index == 1)
	{
		a = 1;
		b = 0;
		c = -image.height() / 2;
		Raster(-c, 0, -c, -c * 2 - 1);
	}
	else if (index == 2)
	{
		a = 1;
		b = 1;
		c = -image.height();
		Raster(0, -c - 1, -c - 1, 0);
	}
	else if (index == 3)
	{
		a = -1;
		b = 1;
		c = 0;
		Raster(0, 0, 399, 399);
	}

	for (int i = 0; i < vrcholy.size(); i++) {
		xx = vrcholy[i].x() - ((float)(2 * a * (a * vrcholy[i].x() + b * vrcholy[i].y() + c)) / (a*a + b * b));
		yy = vrcholy[i].y() - ((float)(2 * b * (a * vrcholy[i].x() + b * vrcholy[i].y() + c)) / (a*a + b * b));
		vrcholy[i] = QPoint(xx, yy);
	}
	Kresli_obrys(red, green, blue);
}


void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	hrany.clear();
	aktivne_hrany.clear();
	vrcholy.clear();
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		vrcholy << lastPoint;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

